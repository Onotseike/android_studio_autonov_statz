package autonov.app.paula.onotseike.autonovstatistics;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.Calendar;


public class MainActivity extends ActionBarActivity {
    private static final String TAG = "MainActivity";


    private int mHour;
    private int mMinute;
    private int mSeconds;
    private float mMillis;

    private int mYear;
    private int mMonth;
    private int mDay;

    private TextView mTempRead;
    private TextView mCurRead;
    private TextView mVoltRead;
    private TextView mSoCRead;


    private Button blueTooth;


    //private BluetoothAdapter mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //Button for bluetooth , email and speech
        final Button email = (Button) findViewById(R.id.email);
        blueTooth = (Button) findViewById(R.id.bluetooth);
        final Button speech = (Button) findViewById(R.id.speech);

        //Display for the current, temperature,voltage and soc reads
        mTempRead = (TextView) findViewById(R.id.temp_data);
        mCurRead = (TextView) findViewById(R.id.current_data);
        mVoltRead = (TextView) findViewById(R.id.volt_data);
        mSoCRead = (TextView) findViewById(R.id.soc_data);

        //Bluetooth Set up Show Devices

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }

        //On Click Listeners for the three buttons
        email.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent sendIntent = new Intent(Intent.ACTION_SEND);
                //sendIntent.setType("plain/text");
                String Message1 = Message_Maker();


                //sendIntent.setClassName("com.google.android.gm", "com.android.mail.compose.ComposeActivity");
                sendIntent.putExtra(Intent.EXTRA_EMAIL, new String[] {
                                "onotseike@hotmail.com"
                        });
                sendIntent.putExtra(Intent.EXTRA_SUBJECT, "AUTONOV STATISTICS ");
                sendIntent.putExtra(Intent.EXTRA_TEXT, Message1);
                sendIntent.setType("message/rfc822");
                startActivity(Intent.createChooser(sendIntent, "Choose an Email client :"));
                
            }
        });

        blueTooth.setOnClickListener( new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent blue = new Intent(getApplicationContext(),ShowDevices.class);
                startActivity(blue);


            }
        });


    }



    private String Message_Maker() {
        String Message = "AUTONOV STATISTICS FOR : "+ "\n";
        // get Time
        final Calendar cali = Calendar.getInstance();
        mHour = cali.get(Calendar.HOUR_OF_DAY);
        mMinute = cali.get(Calendar.MINUTE);
        mSeconds = cali.get(Calendar.SECOND);
        mMillis = cali.get(Calendar.MILLISECOND);

        mYear = cali.get(Calendar.YEAR);
        mMonth = cali.get(Calendar.MONTH);
        mDay = cali.get(Calendar.DAY_OF_MONTH);

        Message += new StringBuilder().append(mDay).append(" /").append(mMonth + 1).append(":")
                .append(mYear).append("  ").append(mHour).append(" : ").append(mMinute)
                .append(" : ").append(mSeconds).append(" : ").append(mMillis)
                .append(" Temperature:  ").append("12 C").append(" Current: ").append("1.5A").append(" Voltage: ").append("5V").append(" SOC: ").append("12%").append("\n").toString();
        //print(Message);
        return Message;
        }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_main, container, false);
            return rootView;
        }
    }
}
